#!/bin/bash

i=1
step=1
MAX_VAL=11
echo "" > ./average_latency.dat 
while [ $i -lt $MAX_VAL ]
do
	k=1
	while [ $k -lt $((i + 1)) ]
	do
		file="./data/test_sub$((k))_$i.txt"
  		echo "$i $(tail $file | grep -Eo "time: [0-9]+" | grep -Eo "[0-9]+")" >> ./average_latency.dat
  		k=$((k + 1))
	done
  i=$((i + step))
done

