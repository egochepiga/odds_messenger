#!/bin/bash

i=0
step=1
MAX_VAL=11
while [ $i -lt $MAX_VAL ]
do
  ../../../publisher -DCPSConfigFile ../../../rtps.ini -s 0 -n 100 --interval 0
  i=$((i + step))
  sleep 2
done
