#!/bin/bash

i=1
step=1
MAX_VAL=11
echo "" > ./all_latency.dat 
while [ $i -lt $MAX_VAL ]
do
	k=1
	while [ $k -lt $((i + 1)) ]
	do
		file="./data/test_sub$((k))_$i.txt"
  		while IFS= read -r line ; do
   			echo "$k $line" >> ./all_latency.dat
  		done <  <(head -100 "$file")
  		k=$((k + 1))
	done
  	i=$((i + step))
done

