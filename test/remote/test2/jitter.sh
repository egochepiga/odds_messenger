#!/bin/bash

i=1
step=1
MAX_VAL=11
echo "" > ./jitter.dat 
while [ $i -lt $MAX_VAL ]
do
  k=1
  while [ $k -lt $((i + 1)) ]
  do
    file="./data/test_sub$((k))_$i.txt"
    average_time=$(tail $file | grep -Eo "time: [0-9]+" | grep -Eo "[0-9]+")
      while IFS= read -r line ; do
        if [ $line -gt $((average_time * 120/100)) ]
        then
          echo "$k $line" >> ./jitter.dat
        fi
      done <  <(head -100 "$file")
      k=$((k + 1))
  done
    i=$((i + step))
done

