#!/bin/bash

i=0
step=1000
MAX_VAL=1000000
echo "" > ./all_latency.dat
while [ $i -lt $MAX_VAL ]
do
  file="./data/test_$i.txt"
  average_time=$(tail $file | grep -Eo "time: [0-9]+" | grep -Eo "[0-9]+")

  msg_count=100
  msg_counter=1
  while IFS= read -r line ; do
   echo "$((i/1000)) $line" >> ./all_latency.dat
   msg_counter=$((msg_counter + 1))
  done <  <(head -100 "$file")
  i=$((i + step))
done

