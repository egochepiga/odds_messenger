#!/bin/bash

i=0
step=1000
MAX_VAL=1000000
echo "" > ./average_latency.dat 
while [ $i -lt $MAX_VAL ]
do
  file="./data/test_$i.txt"
  echo "$((i/1000)) $(tail $file | grep -Eo "time: [0-9]+" | grep -Eo "[0-9]+")" >> ./average_latency.dat
  i=$((i + step))
done

