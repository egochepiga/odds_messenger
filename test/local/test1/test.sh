#!/bin/bash
odds_env
i=400000
step=1000
MAX_VAL=1000000
while [ $i -lt $MAX_VAL ]
do
  ../../../publisher -DCPSConfigFile ../../../rtps.ini -s $i -n 100 --interval 50 & ../../../subscriber -DCPSConfigFile ../../../rtps.ini > "./data/test_$i.txt"
  i=$((i + step))
done