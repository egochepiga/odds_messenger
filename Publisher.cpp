/*
 *
 *
 * Distributed under the OpenDDS License.
 * See: http://www.opendds.org/license.html
 */

#include <ace/Log_Msg.h>

#include <dds/DdsDcpsInfrastructureC.h>
#include <dds/DdsDcpsPublicationC.h>

#include <dds/DCPS/Marked_Default_Qos.h>
#include <dds/DCPS/Service_Participant.h>
#include <dds/DCPS/WaitSet.h>

#include "dds/DCPS/StaticIncludes.h"

#include "MessengerTypeSupportImpl.h"

#include <chrono>
#include <iostream>

#include <thread>
#include <functional>

using namespace std;

thread timer_start(const function<void(void)> &func, unsigned int interval)
{
    thread thread = ::thread([func, interval]() {
            func();
            this_thread::sleep_for(chrono::milliseconds(interval));
    });
    return thread;
}

void testFree(Messenger::Message* messagePtr, Messenger::MessageDataWriter_var message_writer, int interval ) {
    Messenger::Message message = *messagePtr;
    auto timer = timer_start([&]() -> void {

        //std::chrono::time_point<std::chrono::high_resolution_clock> time = std::chrono::high_resolution_clock::now();
        auto time =
                std::to_string(
                        std::chrono::duration_cast<std::chrono::microseconds>
                                (std::chrono::high_resolution_clock::now().time_since_epoch())
                                .count());

        message.time = time.c_str();
        DDS::ReturnCode_t error = message_writer->write(message, DDS::HANDLE_NIL);
        ++message.count;
        messagePtr -> count = message.count;
        ++message.subject_id;

        if (error != DDS::RETCODE_OK) {
            ACE_ERROR((LM_ERROR,
                    ACE_TEXT("ERROR: %N:%l: main() -")
                              ACE_TEXT(" write returned %d!\n"), error));
        }
    }, interval );
    timer.join();
    return;
}


int ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{
    int pSize, pCount = 0;
    int interval = 1000;
  for (int i = 0; i < argc; ++i){

      if ( strncmp( argv[i], "-s", 2 ) == 0 ) {
          pSize = atoi(argv[i + 1]);
          std::cout << "max package size: " << pSize << std::endl;
      }

      if ( strncmp( argv[i], "-n", 2 ) == 0 ) {
          pCount = atoi(argv[i + 1]);
          std::cout << "package count: " << pCount << std::endl;
      }

      if ( strncmp( argv[i], "--interval", 2 ) == 0 ) {
          interval = atoi(argv[i + 1]);
          std::cout << "interval: " << interval << std::endl;
      }
  }

  try {
    // Initialize DomainParticipantFactory
    DDS::DomainParticipantFactory_var dpf =
      TheParticipantFactoryWithArgs(argc, argv);

    // Create DomainParticipant
    DDS::DomainParticipant_var participant =
      dpf->create_participant(42,
                              PARTICIPANT_QOS_DEFAULT,
                              0,
                              OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!participant) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" create_participant failed!\n")),
                       -1);
    }

    // Register TypeSupport (Messenger::Message)
    Messenger::MessageTypeSupport_var ts =
      new Messenger::MessageTypeSupportImpl;

    if (ts->register_type(participant, "") != DDS::RETCODE_OK) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" register_type failed!\n")),
                       -1);
    }

    // Create Topic (Movie Discussion List)
    CORBA::String_var type_name = ts->get_type_name();
    DDS::Topic_var topic =
      participant->create_topic("Movie Discussion List",
                                type_name,
                                TOPIC_QOS_DEFAULT,
                                0,
                                OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!topic) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" create_topic failed!\n")),
                       -1);
    }

    // Create Publisher
    DDS::Publisher_var publisher =
      participant->create_publisher(PUBLISHER_QOS_DEFAULT,
                                    0,
                                    OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!publisher) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" create_publisher failed!\n")),
                       -1);
    }

    // Create DataWriter

      DDS::DataWriterQos dw_qos;
      publisher->get_default_datawriter_qos (dw_qos);
      dw_qos.history.kind = DDS::KEEP_LAST_HISTORY_QOS;
      dw_qos.history.depth = 500;
      dw_qos.reliability.kind = DDS::RELIABLE_RELIABILITY_QOS;
      dw_qos.reliability.max_blocking_time.sec = 1;
      dw_qos.reliability.max_blocking_time.nanosec = 0;
      dw_qos.resource_limits.max_samples_per_instance = 500;

    DDS::DataWriter_var writer =
      publisher->create_datawriter(topic,
                                   dw_qos,
                                   0,
                                   OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!writer) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" create_datawriter failed!\n")),
                       -1);
    }

    Messenger::MessageDataWriter_var message_writer =
      Messenger::MessageDataWriter::_narrow(writer);

    if (!message_writer) {
      ACE_ERROR_RETURN((LM_ERROR,
                        ACE_TEXT("ERROR: %N:%l: main() -")
                        ACE_TEXT(" _narrow failed!\n")),
                       -1);
    }

    // Block until Subscriber is available
    DDS::StatusCondition_var condition = writer->get_statuscondition();
    condition->set_enabled_statuses(DDS::PUBLICATION_MATCHED_STATUS);

    DDS::WaitSet_var ws = new DDS::WaitSet;
    ws->attach_condition(condition);

    while (true) {

        DDS::ConditionSeq conditions;
        DDS::Duration_t timeout = { 60, 0 };
        if (ws->wait(conditions, timeout) != DDS::RETCODE_OK) {
            ACE_ERROR_RETURN((LM_ERROR,
                    ACE_TEXT("ERROR: %N:%l: main() -")
            ACE_TEXT(" wait failed!\n")), -1);
        }

        DDS::PublicationMatchedStatus matches;
        if (writer->get_publication_matched_status(matches) != ::DDS::RETCODE_OK) {
            ACE_ERROR_RETURN((LM_ERROR,
                          ACE_TEXT("ERROR: %N:%l: main() -")
                          ACE_TEXT(" get_publication_matched_status failed!\n")),
                         -1);
        }

        if (matches.current_count >= 1) {
            break;
        }

    }


    // Write samples
    Messenger::Message message;
    message.subject_id = 0;

    message.from       = "Comic Book Guy";
    message.subject    = "Review";
    message.text       = "Worst. Movie. Ever.";
    message.count      = 0;

          message.text = std::string(pSize, '.').c_str();
          std::cout << "Package size: " << pSize << " bytes" << std::endl;
          //std::cout << "Message text: " << message.text << "\n" << std::endl;

      for (int i = 0; i < pCount; ++i) {
          testFree(&message, message_writer, interval);
      }

      std::cout << "____________________________ \n" << std::endl;

      // Wait for samples to be acknowledged
      DDS::Duration_t timeout = {3, 0 };
      if (message_writer->wait_for_acknowledgments(timeout) != DDS::RETCODE_OK) {
          ACE_ERROR_RETURN((LM_ERROR,
                  ACE_TEXT("ERROR: %N:%l: main() -")
                                   ACE_TEXT(" wait_for_acknowledgments failed!\n")),
                           -1);
      }

    // Clean-up!
    participant->delete_contained_entities();
    dpf->delete_participant(participant);

    TheServiceParticipant->shutdown();

  } catch (const CORBA::Exception& e) {
    e._tao_print_exception("Exception caught in main():");
    return -1;
  }

  return 0;
}


