/*
 *
 *
 * Distributed under the OpenDDS License.
 * See: http://www.opendds.org/license.html
 */

#include <ace/Log_Msg.h>
#include <ace/OS_NS_stdlib.h>

#include "DataReaderListenerImpl.h"
#include "MessengerTypeSupportC.h"
#include "MessengerTypeSupportImpl.h"

#include <iostream>
#include <chrono>
#include <map>
#include <vector>

void
DataReaderListenerImpl::on_requested_deadline_missed(
        DDS::DataReader_ptr /*reader*/,
        const DDS::RequestedDeadlineMissedStatus& /*status*/)
{
}

void
DataReaderListenerImpl::on_requested_incompatible_qos(
        DDS::DataReader_ptr /*reader*/,
        const DDS::RequestedIncompatibleQosStatus& /*status*/)
{
}

void
DataReaderListenerImpl::on_sample_rejected(
        DDS::DataReader_ptr /*reader*/,
        const DDS::SampleRejectedStatus& /*status*/)
{
}

int allCount = 0;
int integrityChecker = 0;
int packetSize = 0;
int lastPacketSize = 0;
std::map< int, std::vector<long int> > vectorsForPacketSize;
std::vector<long int> timestamps;


void
DataReaderListenerImpl::on_liveliness_changed(
        DDS::DataReader_ptr /*reader*/,
        const DDS::LivelinessChangedStatus& status)
{

}


void
DataReaderListenerImpl::on_data_available(DDS::DataReader_ptr reader)
{
    Messenger::MessageDataReader_var reader_i =
            Messenger::MessageDataReader::_narrow(reader);

    if (!reader_i) {
        ACE_ERROR((LM_ERROR,
                ACE_TEXT("ERROR: %N:%l: on_data_available() -")
                          ACE_TEXT(" _narrow failed!\n")));
        ACE_OS::exit(-1);
    }

    Messenger::Message message;
    DDS::SampleInfo info;

    DDS::ReturnCode_t error = reader_i->take_next_sample(message, info);



    if (error == DDS::RETCODE_OK) {

        if (info.valid_data) {

            allCount++;
            packetSize = strlen(message.text.in());
            int messageId = message.count;
            long int microSecondsNow = std::chrono::duration_cast< std::chrono::microseconds >
                    (std::chrono::high_resolution_clock::now().time_since_epoch())
                    .count();

            char *end;
            long int microSeconds = std::strtol(message.time.in(), &end, 10);
            long int delay = microSecondsNow - microSeconds;
            std::cout << delay << std::endl;

            timestamps.push_back(delay);

        }

    } else {
        ACE_ERROR((LM_ERROR,
                ACE_TEXT("ERROR: %N:%l: on_data_available() -")
                          ACE_TEXT(" take_next_sample failed!\n")));
    }
}

void
DataReaderListenerImpl::on_subscription_matched(
        DDS::DataReader_ptr /*reader*/,
        const DDS::SubscriptionMatchedStatus& /*status*/)
{
}

void
DataReaderListenerImpl::on_sample_lost(
        DDS::DataReader_ptr /*reader*/,
        const DDS::SampleLostStatus& /*status*/)
{
}
